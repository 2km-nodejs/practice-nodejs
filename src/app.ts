import express from 'express';
import allRoutes from 'express-list-endpoints';

const app = express();
const port = process.env.PORT || 3000;

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.listen(port, () => {
  console.log(`Server is listening on ${port}`);
  console.log(allRoutes(app));
});
